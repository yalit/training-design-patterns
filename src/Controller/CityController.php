<?php

namespace App\Controller;

use App\Weather\FetchWeatherHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CityController extends AbstractController
{
    /**
     * @Route("/city", name="city")
     */
    public function index()
    {
        return $this->render('city/index.html.twig', [
            'controller_name' => 'CityController',
        ]);
    }

    /**
     * @Route("/city/{city}", name="city")
     * @var string $city
     * @var FetchWeatherHandler $weatherHandler
     *
     * @return Response
     */
    public function cityWeather(string $city, FetchWeatherHandler $weatherHandler)
    {
        $weather = $weatherHandler->fetch($city);

        return $this->render('city/city.html.twig', [
            'weather' => $weather,
        ]);
    }
}
