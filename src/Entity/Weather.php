<?php

namespace App\Entity;

class Weather
{
    /**
     * @var Temperature
     */
    private $temperature;

    /**
     * @var Humidity
     */
    private $humidity;

    /**
     * @var Wind
     */
    private $wind;

    public function __construct(Temperature $temperature, Humidity $humidity, ?Wind $wind)
    {
        $this->temperature = $temperature;
        $this->humidity = $humidity;
        $this->wind = $wind;
    }


}