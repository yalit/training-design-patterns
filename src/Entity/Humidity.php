<?php

namespace App\Entity;

class Humidity
{
    private $humidity;

    public function __construct(float $humidity)
    {
        $this->humidity = $humidity;
    }
    public function getValue(): float
    {
        return $this->humidity;
    }

    public function equals(self $other): bool
    {
        return $this->humidity == $other->getValue();
    }

}