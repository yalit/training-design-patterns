<?php

namespace App\Entity;

class Temperature
{
    private $temperature;

    public function __construct(float $temperature)
    {
        $this->temperature = $temperature;
    }

    public function getValue(): float
    {
        return $this->temperature;
    }

    public function equals(self $other): bool
    {
        return $this->temperature = $other->getValue();
    }

}