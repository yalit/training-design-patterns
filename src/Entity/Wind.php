<?php

namespace App\Entity;

class Wind
{
    private $strength;

    public function __construct(float $strength)
    {
        $this->strength = $strength;
    }

    public function getValue(): float
    {
        return $this->strength;
    }

    public function equals(self $other): bool
    {
        return $this->strength == $other->getValue();
    }

}