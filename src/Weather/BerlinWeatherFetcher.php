<?php

namespace App\Weather;

use App\Entity\Humidity;
use App\Entity\Temperature;
use App\Entity\Wind;
use App\Entity\Weather;

class BerlinWeatherFetcher extends AbstractWeatherFetcher
{
    public function __construct()
    {
        $this->specificUrlArgument = 'berlin.json';
    }

    public function getDataFromJson($datas) : Weather
    {
        $datas = json_decode($datas);

        return new Weather(
            new Temperature(floatval($datas->measure->temperature)),
            new Humidity(floatval($datas->measure->humidity)),
            new Wind(floatval($datas->measure->wind))
        );
    }

    public function getName(): string
    {
        return 'berlin';
    }

}