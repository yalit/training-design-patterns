<?php
/**
 * Created by PhpStorm.
 * User: yannick
 * Date: 2019-03-26
 * Time: 23:22
 */

namespace App\Weather;


use App\Entity\Weather;

class FetchWeatherHandler
{
    /**
     * @var CityWeatherInterface[]
     */
    private $fetchers = [];

    /**
     * FetchWeatherHandler constructor.
     * @param CityWeatherInterface[] $fetchers
     */
    public function __construct(iterable $fetchers)
    {
        foreach ($fetchers as $fetcher){
            $this->fetchers[$fetcher->getName()] = $fetcher;
        }
    }

    public function fetch(string $city): Weather
    {
        return $this->fetchers[$city]->fetch();
    }
}