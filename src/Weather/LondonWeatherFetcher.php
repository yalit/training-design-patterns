<?php

namespace App\Weather;

use App\Entity\Humidity;
use App\Entity\Temperature;
use App\Entity\Wind;
use App\Entity\Weather;

class LondonWeatherFetcher extends AbstractWeatherFetcher
{
    public function __construct()
    {
        $this->specificUrlArgument = 'london.json';
    }

    public function getDataFromJson($datas) : Weather
    {
        $datas = json_decode($datas);

        return new Weather(new Temperature(floatval($datas->temperature)), new Humidity(floatval($datas->humidity)), null);
    }

    public function getName(): string
    {
        return 'london';
    }

}