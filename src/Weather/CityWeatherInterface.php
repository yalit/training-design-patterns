<?php
namespace App\Weather;

use App\Entity\Weather;

interface CityWeatherInterface
{

    public function fetch(): Weather;
    public function getName(): string;
}