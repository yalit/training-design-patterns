<?php

namespace App\Weather;

use App\Entity\Humidity;
use App\Entity\Temperature;
use App\Entity\Wind;
use App\Entity\Weather;

class ParisWeatherFetcher extends AbstractWeatherFetcher
{
    public function __construct()
    {
        $this->specificUrlArgument = 'paris.json';
    }

    public function getDataFromJson($datas) : Weather
    {
        $datas = json_decode($datas);

        return new Weather(new Temperature(floatval($datas->temperature)), new Humidity(floatval($datas->humidity)), new Wind(floatval($datas->wind)));
    }

    public function getName(): string
    {
        return 'paris';
    }

}