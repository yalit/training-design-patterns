<?php

namespace App\Weather;


use App\Entity\Weather;

abstract  class AbstractWeatherFetcher implements CityWeatherInterface
{
    protected $baseUrl = 'http://meteo.titouangalopin.com/';
    protected $specificUrlArgument;

    final function getUrl()
    {
        return $this->baseUrl.$this->specificUrlArgument;
    }

    final function fetchJson()
    {
        return file_get_contents($this->getUrl());
    }

    final function fetch(): Weather
    {
        $datas = $this->fetchJson();
        return $this->getDataFromJson($datas);
    }

    abstract function getDataFromJson($datas): Weather;
}